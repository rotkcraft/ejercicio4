package estructuras;


import estructuras.simple.Pila;

/**
 * ^
 * Created by hadexexplade on 17/06/15.
 */
public class Equacion
{
    public Equacion()
    {

    }

    public double resolver(String equacion)
    {
        Pila<Double> valor = new Pila<>();
        Pila<Character> op = new Pila<>();
        int c = 0;
        while (c < equacion.length())
        {
            boolean negativo = false;
            if (equacion.charAt(c) == ' ')
            {
                c++;
            }

            if (equacion.charAt(c) == '-')
            {
                if (c ==0 || equacion.charAt(c - 1) == '(' || equacion.charAt(c - 1) == '-' || equacion.charAt(c - 1) == '+' || equacion.charAt(c - 1) == '*' || equacion.charAt(c - 1) == '/')
                {
                    negativo = true;
                }
            }
            if (Character.isDigit(equacion.charAt(c)) || negativo)
            {
                StringBuilder sb = new StringBuilder();
                if (negativo)
                {
                    sb.append(equacion.charAt((c++)));
                }
                while (c < equacion.length() && Character.isDigit(equacion.charAt(c)))
                {
                    sb.append(equacion.charAt(c++));
                    if (c<equacion.length() && equacion.charAt(c) == '.')
                    {
                        sb.append(equacion.charAt((c++)));
                    }
                }
                c--;
                valor.apilar(Double.parseDouble(sb.toString()));
            }
            else if (equacion.charAt(c) == '(')
            {
                op.apilar(equacion.charAt(c));

            }
            else if (equacion.charAt(c) == ')')
            {
                while (op.primero()!='(')
                {
                    valor.apilar(realizarOperacion(op.desapilar(), valor.desapilar(), valor.desapilar()));
                }
                op.desapilar();
            }
            else if (equacion.charAt(c) == '+' || equacion.charAt(c) == '-' || equacion.charAt(c) == '*'|| equacion.charAt(c) == '^' || equacion.charAt(c) == '/' )
            {
                while (!op.estaVacia() && comprobarPrioridad(equacion.charAt(c), op.primero()))
                {
                    System.out.println(op.primero()+"  "+valor.primero());
                    valor.apilar(realizarOperacion(op.desapilar(), valor.desapilar(), valor.desapilar()));
                }
                op.apilar(equacion.charAt(c));

            }
            c++;
        }
        while (!op.estaVacia())
        {
            valor.apilar(realizarOperacion(op.desapilar(), valor.desapilar(), valor.desapilar()));
        }
        return valor.desapilar();
    }

    private boolean comprobarPrioridad(char operador1, char operador2)
    {
        if (operador2 == '(' || operador2 == ')')
        {
            return false;
        }
        else if ((operador1 == '*' || operador1 == '/' || operador1 == '^') && (operador2 == '+' || operador2 == '-'))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private double realizarOperacion(char operador, double b, double a)
    {
        switch (operador)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '^':
                return Math.pow(a, b);
            case '/':
                if (b == 0)
                {
                    break;
                }
                return a / b;
        }

        return 0;
    }

}
