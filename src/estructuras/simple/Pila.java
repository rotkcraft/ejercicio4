package estructuras.simple;

import java.util.NoSuchElementException;

/**
 * Creado por rotkcraft el 06 de junio del 2015 .
 */
public class Pila<E>
{
    Nodo<E> primero;
    int cantidad;

    public Pila()
    {
        this.cantidad = 0;
    }

    public int getT()
    {
        return this.cantidad;
    }

    public boolean estaVacia()
    {
        return (primero == null);
    }

    public void apilar(E dato)
    {
        primero = new Nodo<E>(dato, primero);
        cantidad++;
    }

    public E primero()
    {
        if (!estaVacia())
        {
            E dato = primero.getInfo();
            return dato;

        }
        throw new NoSuchElementException("Pila Vacia");
    }

    public E desapilar()
    {
        if (!estaVacia())
        {
            E dato = primero.getInfo();
            primero = primero.getSiguiente();
            cantidad--;
            if (cantidad == 0)
            {
                borrar();
            }
            return dato;

        }
        throw new NoSuchElementException("Pila Vacia");
    }

    public void borrar()
    {
        primero = null;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (estaVacia())
        {
            sb.append("]");
            return sb.toString();
        }
        Nodo<E> nodo = primero;
        while (nodo != null)
        {
            sb.append(nodo.getInfo() + ((nodo.getSiguiente() != null) ? ", " : "]"));
            nodo = nodo.getSiguiente();


        }
        return sb.toString();
    }
}
