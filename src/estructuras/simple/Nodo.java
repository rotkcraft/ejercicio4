package estructuras.simple;

/**
 * Creado por rotkcraft el 06 de junio del 2015 .
 */
public class Nodo<E>
{
    private E info;
    private Nodo<E> siguiente;

    public Nodo(E info, Nodo<E> siguiente)
    {
        this.info = info;
        this.siguiente = siguiente;
    }

    public E getInfo()
    {
        return info;
    }

    public void setInfo(E info)
    {
        this.info = info;
    }

    public Nodo<E> getSiguiente()
    {
        return siguiente;
    }

    public void setSiguiente(Nodo<E> siguiente)
    {
        this.siguiente = siguiente;
    }
}
